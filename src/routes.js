import Home from './routes/Home.svelte';
import Page from './routes/Page.svelte';
import History from './routes/History.svelte';
import Visit from './routes/Visit.svelte';
import NotFound from './routes/NotFound.svelte';

export default {
    '/': Home,
    '/histoire': History,
    '/sejour/:id': Visit,
    '/:path': Page,
    // The catch-all route must always be last
    '*': NotFound
};
