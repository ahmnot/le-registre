import { writable } from 'svelte/store';
function createStateStore() {
    const initState = 0;
    const maxState = 2;
    const initStateStore = {
        state: initState,
        direction: 'up'
    }
    const { subscribe, set, update } = writable(initStateStore);

    return {
        subscribe,
        increment: () => update(stateStore => {
            if (stateStore.direction == 'up') {
                if (stateStore.state < maxState) {
                    stateStore.state += 1
                }
                else if (stateStore.state >= maxState) {
                    stateStore.direction = 'down'
                    stateStore.state -= 1
                }
            }
            else if (stateStore.direction == 'down') {
                if (stateStore.state > 1) {
                    stateStore.state -= 1
                }
                else if (stateStore.state <= 1) {
                    stateStore.direction = 'up'
                    stateStore.state += 1
                }
            }
            return stateStore
        }),
        jumpTo: (stateToJumpTo) => update(stateStore => {
            if (stateToJumpTo >= 0 && stateToJumpTo <= maxState) {
                stateStore.state = stateToJumpTo
            }
            return stateStore
        }),
        reset: () => set(initStateStore)
    }
}
export const stateStore = createStateStore()