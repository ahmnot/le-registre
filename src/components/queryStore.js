import {
    isWithinInterval,
    parseISO,
    addDays,
    isAfter,
    compareDesc,
} from "date-fns";

import { writable, derived } from 'svelte/store';
import {
    timelineStore,
} from "./timeline/timelineStore";
function createQueryStore() {
    const queryStore = {
        isFetching: false,
        error: "",
        data: []
    };
    const { subscribe, set, update } = writable(queryStore)
    return {
        subscribe,
        update,
        updateData: (queryData) => update((query) => {
            //console.log("queryData", queryData);
            query.data = queryData
            return query
        }),
        isFetching: (isFetching) => update((query) => {
            query.isFetching = isFetching
            return query
        }),
        setError: (error) => update((query) => {
            query.error = error
            return query
        }),
        reset: () => set(timelineStore),
    }
}
export const queryStore = createQueryStore();

export const filteredData = derived([queryStore, timelineStore], ([$queryStore, $timelineStore]) => {
    return $queryStore.data.filter(
        (e) => {
            // If all dates are defined, we check if the date is in visits data interval
            if (
                e.data.isEndDate == "true" &&
                e.data.startDate != undefined &&
                e.data.endDate != undefined
            ) {
                return isWithinInterval($timelineStore.date, {
                    start: parseISO(e.data.startDate),
                    end: addDays(parseISO(e.data.endDate), 1),
                });
            }
            // If isEndDate is false, we filter only by startDate
            else if (e.data.isEndDate == "false") {
                return isAfter(
                    $timelineStore.date,
                    parseISO(e.data.startDate)
                );
            }
        }
    ).sort((a, b) =>
        compareDesc(
            parseISO(a.data.startDate),
            parseISO(b.data.startDate)
        ))
})