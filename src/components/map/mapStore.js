import { writable } from "svelte/store";
function createmapStore() {
  const floorsNumber = 6;
  const openRatio = 0.5;
  const defaultFloor = 2;
  const openedHeight = 400;
  const closedHeight = 300;
  const array = new Array(floorsNumber).fill(0).map((e) => ({
    x: 0,
    y: 0,
    focused: false,
  }));
  const mapStore = {
    map: {
      isOpen: false,
      isScrolling: false,
      height: closedHeight,
      activeFloor: defaultFloor,
      isNextFloorDisabled: false,
      isPreviousFloorDisabled: false,
      width: 0,
    },
    floors: array,
    focus: function () {
      for (let index = 0; index < this.floors.length; index++) {
        const element = this.floors[index];
        if (index != this.map.activeFloor) {
          element.focused = false;
        } else {
          element.focused = true;
        }
        this.floors[index] = element;
      }
    },
  };
  const { subscribe, set, update } = writable(mapStore);

  return {
    subscribe,
    open: () =>
      update(function (obj) {
        // TODO : change the method using array.prototype.map() instead of for loop
        for (let index = 0; index < obj.floors.length; index++) {
          const element = obj.floors[index];
          element.y =
            (obj.floors.length - 1 - index) * obj.map.width * openRatio;
          obj.floors[index] = element;
        }
        obj.map.height = openedHeight;
        obj.map.isOpen = true;
        return obj;
      }),
    updateWidth: (width) =>
      update((obj) => {
        obj.map.width = width;
        if (obj.map.isOpen) {
          for (let index = 0; index < obj.floors.length; index++) {
            const element = obj.floors[index];
            element.y =
              (obj.floors.length - 1 - index) * obj.map.width * openRatio;
            obj.floors[index] = element;
          }
        }
        return obj;
      }),
    close: () =>
      update(function (obj) {
        // TODO : change the method using array.prototype.map() instead of for loop
        for (let index = 0; index < obj.floors.length; index++) {
          const element = obj.floors[index];
          element.y = 0;
          element.scale = 1;
          obj.floors[index] = element;
        }
        obj.map.height = closedHeight;
        obj.map.isOpen = false;
        return obj;
      }),
    moveTo: (index) =>
      update(function (obj) {
        if (!obj.map.isScrolling) {
          let floor = document.getElementById("floor-" + index);
          if (floor.classList[2] == "selectable") {
            obj.map.activeFloor = Number(index);
            floor.scrollIntoView({
              behavior: "smooth",
              block: "center",
              inline: "center",
            });
            obj.focus();
            obj.map.isNextFloorDisabled =
              document.getElementById("floor-" + (obj.map.activeFloor + 1))
                .classList[2] != "selectable";
            obj.map.isPreviousFloorDisabled =
              document.getElementById("floor-" + (obj.map.activeFloor - 1))
                .classList[2] != "selectable";
          }
          return obj;
        }
      }),
    scrollStart: () =>
      update(function (obj) {
        obj.map.isScrolling == true;
        return obj;
      }),
    scrollEnd: () =>
      update(function (obj) {
        obj.map.isScrolling == false;
        return obj;
      }),
    increment: () =>
      update(function (obj) {
        let floor = document.getElementById(
          "floor-" + (obj.map.activeFloor + 1)
        );
        if (
          floor.classList[2] == "selectable" &&
          obj.map.activeFloor < obj.floors.length - 1
        ) {
          obj.map.activeFloor += 1;
          floor.scrollIntoView({
            behavior: "smooth",
            block: "center",
            inline: "center",
          });
          obj.focus();
          // TODO: Optimize this method by parsing selectable floor in mapStore
          obj.map.isNextFloorDisabled =
            document.getElementById("floor-" + (obj.map.activeFloor + 1))
              .classList[2] != "selectable";
          obj.map.isPreviousFloorDisabled =
            document.getElementById("floor-" + (obj.map.activeFloor - 1))
              .classList[2] != "selectable";
        }
        return obj;
      }),
    decrement: () =>
      update(function (obj) {
        let floor = document.getElementById(
          "floor-" + (obj.map.activeFloor - 1)
        );
        if (floor.classList[2] == "selectable" && obj.map.activeFloor > 0) {
          obj.map.activeFloor -= 1;
          floor.scrollIntoView({
            behavior: "smooth",
            block: "center",
            inline: "center",
          });
          obj.focus();
          obj.map.isNextFloorDisabled =
            document.getElementById("floor-" + (obj.map.activeFloor + 1))
              .classList[2] != "selectable";
          obj.map.isPreviousFloorDisabled =
            document.getElementById("floor-" + (obj.map.activeFloor - 1))
              .classList[2] != "selectable";
        }
        return obj;
      }),
    reset: () => set(mapStore),
  };
}

export const mapStore = createmapStore();
