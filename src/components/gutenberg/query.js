import { operationStore } from "@urql/svelte";
export function blocksQuery(id) {
  return operationStore(
    `query ($id: ID!) {
        page(idType: URI, id: $id) {
            title
            blocks {
                ... on CoreParagraphBlock {
                    attributes {
                        ... on CoreParagraphBlockAttributes {
                        content
                        fontSize
                        textColor
                        align
                        }
                    }
                }
                ... on CoreImageBlock {
                        attributes {
                            ... on CoreImageBlockAttributes {
                                url
                                width
                                title
                                align
                                caption
                                alt
                        }
                    }
                }
                ... on CoreHeadingBlock {
                    attributes {
                        ... on CoreHeadingBlockAttributes {
                            align
                            anchor
                            backgroundColor
                            className
                            content
                            fontSize
                            textColor
                            level
                            textAlign
                            placeholder
                        }
                    }
                }
                ... on PdfembPdfEmbedderViewerBlock {
                    dynamicContent
                    attributes {
                        url 
                    }
                }
                ... on CoreColumnsBlock {
                    innerBlocks {
                      ... on CoreColumnBlock {
                        attributes {
                          ... on CoreColumnBlockAttributes {
                            width
                            verticalAlignment
                          }
                        }
                        innerBlocks {
                            ... on CoreParagraphBlock {
                                attributes {
                                    ... on CoreParagraphBlockAttributes {
                                        content
                                        fontSize
                                        textColor
                                        align
                                    }
                                }
                            }
                            ... on CoreImageBlock {
                                    attributes {
                                        ... on CoreImageBlockAttributes {
                                            url
                                            width
                                            title
                                            align
                                            caption
                                            alt
                                        }
                                    }
                            }
                            ... on CoreHeadingBlock {
                                attributes {
                                    ... on CoreHeadingBlockAttributes {
                                        align
                                        anchor
                                        backgroundColor
                                        className
                                        content
                                        fontSize
                                        textColor
                                        level
                                        textAlign
                                        placeholder
                                    }
                                }
                            }
                            ... on CoreSpacerBlock {
                                attributes {
                                  height
                                }
                                name
                                dynamicContent
                                originalContent
                              }
                            ... on CoreSeparatorBlock {
                                name
                            }
                        }
                    }
                }
            }
        }
    }
    }`,
    { id }
  );
}
