import { writable, derived } from "svelte/store";
import {
  format,
  startOfMonth,
  endOfMonth,
  addMonths,
  subMonths,
  eachDayOfInterval,
  setDate,
  isSameMonth,
  formatISO,
  getDaysInMonth,
} from "date-fns";

function createTimelineStore() {
  const currentDate = Date.now();
  const timelineStore = {
    interval: {
      start: startOfMonth(currentDate),
      end: endOfMonth(currentDate),
    },
    date: currentDate,
  };
  const { subscribe, set, update } = writable(timelineStore);
  return {
    subscribe,
    update,
    incrementMonth: () =>
      update((timeline) => {
        timeline.date = addMonths(timeline.date, 1);
        timeline.interval = {
          start: startOfMonth(timeline.date),
          end: endOfMonth(timeline.date),
        };
        return timeline;
      }),
    decrementMonth: () =>
      update((timeline) => {
        timeline.date = subMonths(timeline.date, 1);
        timeline.interval = {
          start: startOfMonth(timeline.date),
          end: endOfMonth(timeline.date),
        };
        return timeline;
      }),
    setDate: (value) =>
      update((timeline) => {
        timeline.date = setDate(timeline.date, value);
        if (!isSameMonth(timeline.date, timeline.interval.start)) {
          timeline.interval = {
            start: startOfMonth(timeline.date),
            end: endOfMonth(timeline.date),
          };
        }
        return timeline;
      }),
    setDateAbs: (datePicked) =>
      update((timeline) => {
        timeline.date = datePicked;

        timeline.interval = {
          start: startOfMonth(timeline.date),
          end: endOfMonth(timeline.date),
        };
        return timeline;
      }),
    reset: () => set(timelineStore),
  };
}
export const timelineStore = createTimelineStore();

export const dayInMonth = derived(timelineStore, ($timelineStore) =>
  format($timelineStore.date, "d")
);
export const numberOfDay = derived(timelineStore, ($timelineStore) =>
  eachDayOfInterval($timelineStore.interval)
);
export const numberOfDayInMonth = derived(timelineStore, ($timelineStore) =>
  getDaysInMonth($timelineStore.date)
);
export const intervalFormatted = derived(timelineStore, ($timelineStore) => {
  let interval = {
    start: formatISO($timelineStore.interval.start),
    end: formatISO($timelineStore.interval.end),
  };
  return interval;
});
